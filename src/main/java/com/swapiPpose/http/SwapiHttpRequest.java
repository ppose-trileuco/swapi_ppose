package com.swapiPpose.http;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpMethod;

public class SwapiHttpRequest {

	private String url;

	private HttpMethod method;

	private Map<String, String> params;

	private Map<String, String> headers;

	public static String SEARCH_PARAM = "search";

	public SwapiHttpRequest(String url, HttpMethod method) {
		super();
		this.url = url;
		this.method = method;
	}

	public String getUrl() {
		return url;
	}

	public HttpMethod getMethod() {
		return method;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public SwapiHttpRequest params(Map<String, String> params) {
		if (this.params == null) {
			this.params = new HashMap<String, String>();
		}
		this.params.putAll(params);
		return this;
	}

	public SwapiHttpRequest headers(Map<String, String> headers) {
		if (this.headers == null) {
			this.headers = new HashMap<String, String>();
		}
		this.headers.putAll(headers);
		return this;
	}

	public SwapiHttpRequest searchBy(String searchCriteria) {
		Map<String, String> param = new HashMap<String, String>();
		param.put(SEARCH_PARAM, searchCriteria);
		return params(param);
	}

}
