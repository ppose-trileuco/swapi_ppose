package com.swapiPpose.http;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.swapiPpose.http.exception.SwapiHttpExcepection;

public class HttpUtils {

	private static final CloseableHttpClient HTTP_CLIENT = HttpClients.createDefault();

	private static final Logger LOG = LoggerFactory.getLogger(HttpUtils.class);

	public static <T> T excute(Class<T> classType, SwapiHttpRequest swapiHttpRequest) {

		try {
			HttpEntity httpEntityResponse = getHttpEntityResponse(swapiHttpRequest);
			String response = EntityUtils.toString(httpEntityResponse);
			Gson gson = new Gson();
			return gson.fromJson(response, classType);
		} catch (ClientProtocolException e) {

		} catch (URISyntaxException e) {
			LOG.error("Error adding url param: ", e);
			throw new SwapiHttpExcepection();
		} catch (JsonSyntaxException e) {
			LOG.error("Error when transfom json to object: ", e);
			throw new SwapiHttpExcepection();
		} catch (Exception e) {
			LOG.error("Error when excute request ", e);
			throw new SwapiHttpExcepection();
		}
		return null;
	}

	private static HttpEntity getHttpEntityResponse(SwapiHttpRequest swapiHttpRequest)
			throws URISyntaxException, ClientProtocolException, IOException {

		HttpUriRequest request = null;
		HttpMethod method = swapiHttpRequest.getMethod();
		String url = swapiHttpRequest.getUrl();
		Map<String, String> params = swapiHttpRequest.getParams();

		if (params != null) {
			for (Entry<String, String> entrySet : params.entrySet()) {
				url = new URIBuilder(url).addParameter(entrySet.getKey(), entrySet.getValue()).build().toString();
			}
		}

		switch (method) {
		case GET:
			request = new HttpGet(url);
			break;
		case HEAD:
			request = new HttpHead(url);
			break;
		case OPTIONS:
			request = new HttpOptions(url);
			break;
		case PATCH:
			request = new HttpPatch(url);
			break;
		case POST:
			request = new HttpPost(url);
			break;
		case PUT:
			request = new HttpPut(url);
			break;
		default:
			LOG.error("Error in swapiHttpRequest method type");
			break;
		}

		CloseableHttpResponse response = HTTP_CLIENT.execute(request);
		return response.getEntity();

	}

}
