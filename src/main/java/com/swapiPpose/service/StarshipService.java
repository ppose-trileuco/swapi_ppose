package com.swapiPpose.service;

import com.swapiPpose.pojos.Starship;

public interface StarshipService {

	Starship findStarship(String url);

}
