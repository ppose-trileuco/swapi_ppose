package com.swapiPpose.service;

import com.swapiPpose.pojos.Vehicle;

public interface VehicleService {

	Vehicle findVehicle(String url);
}
