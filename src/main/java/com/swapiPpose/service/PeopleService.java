package com.swapiPpose.service;

import java.util.List;

import com.swapiPpose.pojos.People;
import com.swapiPpose.pojos.Person;

public interface PeopleService {

	List<Person> findPersonsByName(String name);

	People findPeople(String url);

}
