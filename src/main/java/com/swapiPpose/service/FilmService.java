package com.swapiPpose.service;

import com.swapiPpose.pojos.Film;

public interface FilmService {
	Film findFilm(String url);
}
