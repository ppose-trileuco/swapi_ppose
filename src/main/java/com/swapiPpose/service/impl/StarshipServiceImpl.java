package com.swapiPpose.service.impl;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.swapiPpose.cache.Cached;
import com.swapiPpose.http.HttpUtils;
import com.swapiPpose.http.SwapiHttpRequest;
import com.swapiPpose.pojos.Starship;
import com.swapiPpose.service.StarshipService;

@Service
public class StarshipServiceImpl implements StarshipService {

	@Override
	@Cached
	public Starship findStarship(String url) {
		SwapiHttpRequest request = new SwapiHttpRequest(url, HttpMethod.GET);
		return HttpUtils.excute(Starship.class, request);
	}

}
