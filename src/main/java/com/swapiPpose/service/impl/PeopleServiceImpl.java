package com.swapiPpose.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.swapiPpose.cache.Cached;
import com.swapiPpose.http.HttpUtils;
import com.swapiPpose.http.SwapiHttpRequest;
import com.swapiPpose.pojos.People;
import com.swapiPpose.pojos.Person;
import com.swapiPpose.service.ApplicationService;
import com.swapiPpose.service.PeopleService;

@Service
public class PeopleServiceImpl extends ApplicationService implements PeopleService {

	@Override
	public List<Person> findPersonsByName(String name) {

		SwapiHttpRequest request = new SwapiHttpRequest(getBaseUrl() + "/people", HttpMethod.GET).searchBy(name);
		People people = HttpUtils.excute(People.class, request);
		List<Person> persons = new ArrayList<Person>();

		if (!CollectionUtils.isEmpty(people.getPersons())) {
			persons.addAll(people.getPersons());
			while (people.getNext() != null) {
				people = findPeople(people.getNext());
				persons.addAll(people.getPersons());
			}
		}

		return persons;
	}

	@Override
	@Cached
	public People findPeople(String url) {
		SwapiHttpRequest request = new SwapiHttpRequest(url, HttpMethod.GET);
		return HttpUtils.excute(People.class, request);
	}
}
