package com.swapiPpose.service.impl;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.swapiPpose.cache.Cached;
import com.swapiPpose.http.HttpUtils;
import com.swapiPpose.http.SwapiHttpRequest;
import com.swapiPpose.pojos.Vehicle;
import com.swapiPpose.service.VehicleService;

@Service
public class VehicleServiceImpl implements VehicleService {

	@Override
	@Cached
	public Vehicle findVehicle(String url) {
		SwapiHttpRequest request = new SwapiHttpRequest(url, HttpMethod.GET);
		return HttpUtils.excute(Vehicle.class, request);
	}

}
