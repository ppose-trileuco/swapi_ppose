package com.swapiPpose.service.impl;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.swapiPpose.cache.Cached;
import com.swapiPpose.http.HttpUtils;
import com.swapiPpose.http.SwapiHttpRequest;
import com.swapiPpose.pojos.Planet;
import com.swapiPpose.service.PlanetService;

@Service
public class PlanetServiceImpl implements PlanetService {

	@Override
	@Cached
	public Planet findPlanet(String url) {
		SwapiHttpRequest request = new SwapiHttpRequest(url, HttpMethod.GET);
		return HttpUtils.excute(Planet.class, request);
	}

}
