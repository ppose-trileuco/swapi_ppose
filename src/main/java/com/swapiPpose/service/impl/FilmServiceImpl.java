package com.swapiPpose.service.impl;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.swapiPpose.cache.Cached;
import com.swapiPpose.http.HttpUtils;
import com.swapiPpose.http.SwapiHttpRequest;
import com.swapiPpose.pojos.Film;
import com.swapiPpose.service.FilmService;

@Service
public class FilmServiceImpl implements FilmService {

	@Override
	@Cached
	public Film findFilm(String url) {
		SwapiHttpRequest request = new SwapiHttpRequest(url, HttpMethod.GET);
		return HttpUtils.excute(Film.class, request);
	}

}
