package com.swapiPpose.service;

import com.swapiPpose.pojos.Planet;

public interface PlanetService {

	Planet findPlanet(String url);
}
