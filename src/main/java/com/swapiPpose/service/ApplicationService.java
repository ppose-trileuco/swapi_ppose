package com.swapiPpose.service;

public abstract class ApplicationService {

	private static final String BASE_URL = "http://swapi.trileuco.com:1138/api";

	public static String getBaseUrl() {
		return BASE_URL;
	}
}
