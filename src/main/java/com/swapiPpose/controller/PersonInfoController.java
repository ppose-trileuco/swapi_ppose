package com.swapiPpose.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.swapiPpose.controller.helper.PersonInfoHelper;
import com.swapiPpose.pojos.PersonInfo;

@RestController
@RequestMapping(value = "/swapi-proxy")
public class PersonInfoController {

	private static final Logger LOG = LoggerFactory.getLogger(PersonInfoController.class);

	@Autowired
	PersonInfoHelper personInfoHelper;

	@GetMapping("/person-info")
	public ResponseEntity<?> personInfo(@RequestParam(required = false) String name) {

		if (StringUtils.isEmpty(name)) {
			return new ResponseEntity<>("Invalid parameters", HttpStatus.BAD_REQUEST);

		}

		List<PersonInfo> personInfos = null;
		try {
			personInfos = personInfoHelper.getPersonInfos(name);
		} catch (Exception e) {
			LOG.error("Error getting personInfos ", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (CollectionUtils.isEmpty(personInfos)) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<PersonInfo>>(personInfos, HttpStatus.OK);
		}
	}

}
