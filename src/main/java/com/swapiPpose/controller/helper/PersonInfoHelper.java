package com.swapiPpose.controller.helper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.swapiPpose.pojos.Film;
import com.swapiPpose.pojos.FilmInfo;
import com.swapiPpose.pojos.Person;
import com.swapiPpose.pojos.PersonInfo;
import com.swapiPpose.pojos.Planet;
import com.swapiPpose.pojos.Starship;
import com.swapiPpose.pojos.Transport;
import com.swapiPpose.pojos.Vehicle;
import com.swapiPpose.service.FilmService;
import com.swapiPpose.service.PeopleService;
import com.swapiPpose.service.PlanetService;
import com.swapiPpose.service.StarshipService;
import com.swapiPpose.service.VehicleService;

@Component
public class PersonInfoHelper {

	@Autowired
	PeopleService peopleService;

	@Autowired
	FilmService filmService;

	@Autowired
	PlanetService planetService;

	@Autowired
	StarshipService starshipService;

	@Autowired
	VehicleService vehicleService;

	public List<PersonInfo> getPersonInfos(String name) {

		List<PersonInfo> personInfos = new ArrayList<PersonInfo>();
		List<Person> persons = peopleService.findPersonsByName(name);
		if (!CollectionUtils.isEmpty(persons)) {
			for (Person person : persons) {
				personInfos.add(buidPersonInfo(person));
			}
		}
		return personInfos;
	}

	private PersonInfo buidPersonInfo(Person person) {

		PersonInfo personInfo = new PersonInfo();

		String name = person.getName();
		if (!StringUtils.isEmpty(name)) {
			personInfo.setName(name);
		}

		String birthYear = person.getBirthYear();
		if (!StringUtils.isEmpty(birthYear)) {
			personInfo.setBirthYear(birthYear);
		}

		String gender = person.getGender();
		if (!StringUtils.isEmpty(gender)) {
			personInfo.setGender(gender);
		}

		String homeworldUrl = person.getHomeworldUrl();
		if (!StringUtils.isEmpty(homeworldUrl)) {
			Planet planet = planetService.findPlanet(homeworldUrl);
			personInfo.setPlanetName(planet.getName());
		}

		List<String> filmUrls = person.getFilmUrls();
		if (!CollectionUtils.isEmpty(filmUrls)) {
			List<FilmInfo> filmInfos = new ArrayList<FilmInfo>();
			for (String filmUrl : filmUrls) {
				Film film = filmService.findFilm(filmUrl);
				FilmInfo filmInfo = new FilmInfo(film.getTitle(), film.getReleaseDate());
				filmInfos.add(filmInfo);
			}
			personInfo.setFilms(filmInfos);
		}

		List<Transport> transports = new ArrayList<Transport>();
		List<String> starshipUrls = person.getStarshipUrls();
		if (!CollectionUtils.isEmpty(starshipUrls)) {
			for (String starshipUrl : starshipUrls) {
				Starship starship = starshipService.findStarship(starshipUrl);
				transports.add(starship);
			}
		}

		List<String> vehicleUrls = person.getVehicleUrls();
		if (!CollectionUtils.isEmpty(vehicleUrls)) {
			for (String vehicleUrl : vehicleUrls) {
				Vehicle vehicle = vehicleService.findVehicle(vehicleUrl);
				transports.add(vehicle);
			}
		}

		if (!CollectionUtils.isEmpty(transports)) {
			Transport maxSpeedTransport = transports.stream()
					.max(Comparator.comparing(Transport::getMaxAtmospheringSpeed)).get();
			if (maxSpeedTransport != null) {
				personInfo.setRapidVehicleDriven(maxSpeedTransport.getName());
			}
		}

		return personInfo;
	}

}
