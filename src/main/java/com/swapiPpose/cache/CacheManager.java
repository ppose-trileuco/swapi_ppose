package com.swapiPpose.cache;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CacheManager extends Thread {

	private static Long timeCache = 600000L;

	public CacheManager() {
		new Cache(timeCache).start();
	}

	@Around(" @annotation(com.swapiPpose.cache.Cached)")
	public Object cached(ProceedingJoinPoint pjp) throws Throwable {

		String key = Cache.generateKey(pjp);
		if (Cache.containsKey(key)) {
			return Cache.get(key);
		} else {
			Object result = pjp.proceed();
			Cache.put(key, result);
			return result;
		}
	}

}
