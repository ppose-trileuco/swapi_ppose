package com.swapiPpose.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Cache extends Thread {

	private static final Logger LOG = LoggerFactory.getLogger(Cache.class);

	private static Map<String, Object> cache = new ConcurrentHashMap<String, Object>();

	private Long millis;

	public Cache(Long millis) {
		super();
		this.millis = millis;
	}

	@Override
	public void run() {
		while (true) {
			try {
				sleep(millis);
				cache.clear();
			} catch (InterruptedException e) {
				LOG.error("error cleanning cache ", e);
			}
		}
	}

	public static Object get(String key) {
		return cache.get(key);
	}

	public static void put(String key, Object o) {
		cache.put(key, o);
	}

	public static boolean containsKey(String key) {
		return cache.containsKey(key);
	}

	public static String generateKey(ProceedingJoinPoint pjp) {

		Signature signature = pjp.getSignature();
		String methodName = signature.getName();
		String className = pjp.getThis().getClass().getName();
		String arguments = "";

		Object[] args = pjp.getArgs();
		if (args != null && args.length > 0) {
			for (Object arg : args) {
				arguments += arg.toString();
			}
		}
		return className + methodName + arguments;
	}

}
