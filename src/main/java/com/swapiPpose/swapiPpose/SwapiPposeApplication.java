package com.swapiPpose.swapiPpose;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = { "com.swapiPpose.controller" })
@ComponentScan(basePackages = { "com.swapiPpose.service" })
@ComponentScan(basePackages = { "com.swapiPpose.cache" })
@SpringBootApplication
public class SwapiPposeApplication {
	public static void main(String[] args) {
		SpringApplication.run(SwapiPposeApplication.class, args);
	}

}
