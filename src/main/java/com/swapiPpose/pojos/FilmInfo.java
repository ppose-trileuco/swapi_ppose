package com.swapiPpose.pojos;

import com.google.gson.annotations.SerializedName;

public class FilmInfo {

	private String name;

	@SerializedName("release_date")
	private String releaseDate;

	public FilmInfo(String name, String releaseDate) {
		super();
		this.name = name;
		this.releaseDate = releaseDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

}
