package com.swapiPpose.pojos;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Person {

	@SerializedName("name")
	@Expose
	private String name;

	@SerializedName("height")
	@Expose
	private String height;

	@SerializedName("mass")
	@Expose
	private String mass;

	@SerializedName("hair_color")
	@Expose
	private String hairColor;

	@SerializedName("skin_color")
	@Expose
	private String skinColor;

	@SerializedName("eye_color")
	@Expose
	private String eyeColor;

	@SerializedName("birth_year")
	@Expose
	private String birthYear;

	@SerializedName("gender")
	@Expose
	private String gender;

	@SerializedName("homeworld")
	@Expose
	private String homeworldUrl;

	@SerializedName("films")
	@Expose
	private List<String> filmUrls;

	@SerializedName("species")
	@Expose
	private List<String> specieUrls;

	@SerializedName("vehicles")
	@Expose
	private List<String> vehicleUrls;

	@SerializedName("starships")
	@Expose
	private List<String> starshipUrls;

	@SerializedName("created")
	@Expose
	private String created;

	@SerializedName("edited")
	@Expose
	private String edited;

	@SerializedName("url")
	@Expose
	private String url;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getMass() {
		return mass;
	}

	public void setMass(String mass) {
		this.mass = mass;
	}

	public String getHairColor() {
		return hairColor;
	}

	public void setHairColor(String hairColor) {
		this.hairColor = hairColor;
	}

	public String getSkinColor() {
		return skinColor;
	}

	public void setSkinColor(String skinColor) {
		this.skinColor = skinColor;
	}

	public String getEyeColor() {
		return eyeColor;
	}

	public void setEyeColor(String eyeColor) {
		this.eyeColor = eyeColor;
	}

	public String getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHomeworldUrl() {
		return homeworldUrl;
	}

	public void setHomeworldUrl(String homeworldUrl) {
		this.homeworldUrl = homeworldUrl;
	}

	public List<String> getFilmUrls() {
		return filmUrls;
	}

	public void setFilmUrls(List<String> filmUrls) {
		this.filmUrls = filmUrls;
	}

	public List<String> getSpecieUrls() {
		return specieUrls;
	}

	public void setSpecieUrls(List<String> specieUrls) {
		this.specieUrls = specieUrls;
	}

	public List<String> getVehicleUrls() {
		return vehicleUrls;
	}

	public void setVehicleUrls(List<String> vehicleUrls) {
		this.vehicleUrls = vehicleUrls;
	}

	public List<String> getStarshipUrls() {
		return starshipUrls;
	}

	public void setStarshipUrls(List<String> starshipUrls) {
		this.starshipUrls = starshipUrls;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getEdited() {
		return edited;
	}

	public void setEdited(String edited) {
		this.edited = edited;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
