package com.swapiPpose.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transport {

	@SerializedName("max_atmosphering_speed")
	@Expose
	private Double maxAtmospheringSpeed;

	@SerializedName("name")
	@Expose
	private String name;

	public Double getMaxAtmospheringSpeed() {
		return maxAtmospheringSpeed;
	}

	public void setMaxAtmospheringSpeed(Double maxAtmospheringSpeed) {
		this.maxAtmospheringSpeed = maxAtmospheringSpeed;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
