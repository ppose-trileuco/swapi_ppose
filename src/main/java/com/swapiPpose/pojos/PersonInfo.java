package com.swapiPpose.pojos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PersonInfo {

	private String name;

	private String birthYear;

	private String gender;

	private String planetName;

	private String rapidVehicleDriven;

	private List<FilmInfo> films;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("birth_year")
	public String getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@JsonProperty("planet_name")
	public String getPlanetName() {
		return planetName;
	}

	public void setPlanetName(String planetName) {
		this.planetName = planetName;
	}

	@JsonProperty("rapid_vehicle_driven")
	public String getRapidVehicleDriven() {
		return rapidVehicleDriven;
	}

	public void setRapidVehicleDriven(String rapidVehicleDriven) {
		this.rapidVehicleDriven = rapidVehicleDriven;
	}

	public List<FilmInfo> getFilms() {
		return films;
	}

	public void setFilms(List<FilmInfo> films) {
		this.films = films;
	}

}
