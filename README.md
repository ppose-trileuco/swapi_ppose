## Instrucciones para la ejecución

1. Clonal el proyecto del repositorio ejecutándo el sigueinte comando: **git clone git@bitbucket.org:ppose-trileuco/swapi_ppose.git**
2. El la carpeta donde se haya clonado ejecutar **./gradlew bootRun**
3. Una vez ejecutado se podrán hacer peticiones para la búsqueda de personajes de star wars por nombre. Ej:
	 **http://localhost:8080/swapi-proxy/person-info?name=yoda**

